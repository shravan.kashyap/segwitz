import React from "react";
import { Link } from "react-router-dom";
export default function Layout(props) {
  console.log("props::", props);
  return (
    <nav className="navbar navbar-expand-md navbar-light border-bottom ">
      <span className="navbar-brand btn">
        <img
          src="https://segwitz.com/wp-content/uploads/2021/01/web-and-mobile-app-company.png"
          //   srcset="https://segwitz.com/wp-content/uploads/2021/01/web-and-mobile-app-company.png 1x, https://segwitz.com/wp-content/uploads/2021/01/web-and-mobile-app-company.png 2x"
          alt="SegWitz"
          style={{ width: 200, padding: 12 }}
        />
      </span>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto py-4 py-md-0">
          {props.currentUser ? (
            <>
              <li className="nav-item">
                <Link to={"/blogs"} className="nav-link text-white">
                  Blogs
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/compose-blog"} className="nav-link text-white">
                  Compose Blog
                </Link>
              </li>
              <li className="nav-item ">
                <span
                  className="nav-link btn text-white "
                  style={{ textAlign: "inherit" }}
                >
                  {props.currentUser.username}
                </span>
              </li>
              <li className="nav-item">
                <span
                  className="nav-link btn text-white"
                  style={{ textAlign: "inherit" }}
                  onClick={(e) => {
                    e.preventDefault();
                    props.logOutUser(e);
                  }}
                >
                  LogOut
                </span>
              </li>
            </>
          ) : (
            <>
              {/* // <div className="navbar-nav ml-auto"> */}
              <li className="nav-item">
                <Link to={"/login"} className="nav-link text-white">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link text-white">
                  Sign Up
                </Link>
              </li>
            </>
          )}
        </ul>
      </div>
    </nav>
  );
}
