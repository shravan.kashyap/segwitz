import { BLOG_SUCCESS, BLOG_FAIL, SET_MESSAGE } from "./types";
import BlogService from "../services/blog.service";

export const handleAddBlog =
  (dataUri, title, description, userName) => (dispatch) => {
    let blogDetail = BlogService.handleAddBlog(
      dataUri,
      title,
      description,
      userName
    );
    if (blogDetail) {
      dispatch({
        type: BLOG_SUCCESS,
        payload: { blogDetail },
      });

      dispatch({
        type: SET_MESSAGE,
        payload: blogDetail.message,
      });

      return Promise.resolve();
    } else {
      dispatch({
        type: BLOG_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: "Blog Add Fail.",
      });

      return Promise.reject();
    }
  };
