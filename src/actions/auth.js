import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  SET_MESSAGE,
} from "./types";

import AuthService from "../services/auth.service";

export const register = (username, email, password) => (dispatch) => {
  let userDetail = AuthService.register(username, email, password);
  if (userDetail) {
    dispatch({
      type: REGISTER_SUCCESS,
    });

    dispatch({
      type: SET_MESSAGE,
      payload: userDetail.message,
    });

    return Promise.resolve();
  } else {
    dispatch({
      type: REGISTER_FAIL,
    });

    dispatch({
      type: SET_MESSAGE,
      payload: "Registration Fail.",
    });

    return Promise.reject();
  }
};

export const login = (username, password) => (dispatch) => {
  let user = AuthService.login(username, password);
  if (user) {
    dispatch({
      type: LOGIN_SUCCESS,
      payload: { user },
    });

    return Promise.resolve();
  } else {
    dispatch({
      type: LOGIN_FAIL,
    });

    dispatch({
      type: SET_MESSAGE,
      payload: "Username or password is wromg",
    });

    return Promise.reject();
  }
};

export const logout = () => (dispatch) => {
  AuthService.logout();

  dispatch({
    type: LOGOUT,
  });
};
