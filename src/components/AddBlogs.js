import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Dropzone from "react-dropzone";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import { handleAddBlog } from "../actions/Blogs";
import Input from "react-validation/build/input";
import Textarea from "react-validation/build/textarea";
import { withRouter } from "react-router";
const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const vusername = (value) => {
  if (value.length < 3 || value.length > 100) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

const AddBlogs = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const [title, settitle] = useState("");
  const [description, setDescription] = useState("");
  const [dataUri, setDataUri] = useState("");
  const [successful, setSuccessful] = useState(false);
  const { message } = useSelector((state) => state.message);
  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const uploadImage = (file) => {
    fileToDataUri(file[0]).then((dataUri) => {
      console.log(dataUri);
      setDataUri(dataUri);
    });
  };

  const fileToDataUri = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        resolve(event.target.result);
      };
      reader.readAsDataURL(file);
    });

  const handleSubmitBlog = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      try {
        dispatch(
          handleAddBlog(dataUri, title, description, currentUser.userName)
        )
          .then(() => {
            setSuccessful(true);
            props.history.push("/blogs");
          })
          .catch(() => {
            setSuccessful(false);
          });
      } catch (er) {
        console.log(er);
      }
    }
  };

  return (
    <div className="card text-white bg-transparent m-0">
      <Form onSubmit={handleSubmitBlog} ref={form}>
        {!successful && (
          <>
            <Dropzone
              accept="image/*"
              onDrop={(acceptedFiles) => uploadImage(acceptedFiles)}
            >
              {({ getRootProps, getInputProps }) => (
                <section
                  className="container border text-center d-flex "
                  style={{
                    width: "100%",
                    height: 200,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <img
                      src={
                        dataUri
                          ? dataUri
                          : "https://www.pngarts.com/files/2/Upload-PNG-Picture.png"
                      }
                      style={{
                        width: "100%",
                        height: 100,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      alt=""
                    ></img>
                  </div>
                </section>
              )}
            </Dropzone>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Title</label>
              <Input
                type="text"
                value={title}
                className="form-control"
                placeholder="Enter title"
                onChange={(e) => settitle(e.target.value)}
                validations={[required, vusername]}
              />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Description</label>
              <Textarea
                className="form-control"
                value={description}
                rows="6"
                placeholder="Enter Description"
                onChange={(e) => setDescription(e.target.value)}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <button className="btn btn-primary btn-block"> Submit</button>
            </div>
          </>
        )}
        {message && (
          <div className="form-group">
            <div
              className={
                successful ? "alert alert-success" : "alert alert-danger"
              }
              role="alert"
            >
              {message}
            </div>
          </div>
        )}{" "}
        <CheckButton style={{ display: "none" }} ref={checkBtn} />
      </Form>
    </div>
  );
};

export default withRouter(AddBlogs);
