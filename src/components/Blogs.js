import React from "react";
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

const Blogs = () => {
  const { user: currentUser } = useSelector((state) => state.auth);
  const { data: blogData } = useSelector((state) => state.auth);

  if (!currentUser) {
    return <Redirect to="/login" />;
  }
  // console.log("blogData::", blogData);
  return (
    <div className="row">
      {!blogData
        ? ""
        : blogData.map((da, i) => {
            if (da.userName === currentUser.userName) {
              return (
                <div className="col-lg-3 col-sm-12 col-md-4" key={i}>
                  <div className="card text-dark" style={{ height: 450 }}>
                    <img
                      src={
                        da.dataUri
                          ? da.dataUri
                          : "https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg"
                      }
                      className="card-img-top"
                      style={{ height: 200, objectFit: "cover" }}
                      alt="..."
                    />
                    <div className="card-body p-0 pt-3">
                      <h5 className="card-title card-title-for">{da.title}</h5>
                      <div className="card-text card-description">
                        {da.description}
                      </div>
                      <span className="btn btn-primary mt-4">Read more</span>
                    </div>
                  </div>
                </div>
              );
            } else {
              return "";
            }
          })}
    </div>
  );
};

export default Blogs;
