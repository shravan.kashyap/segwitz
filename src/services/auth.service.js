const register = (username, email, password) => {
  let useDetails = JSON.parse(localStorage.getItem("useDetails"));
  let dat = { username, email, password };
  if (useDetails) {
    useDetails.push(dat);
  } else {
    useDetails = [dat];
  }
  localStorage.setItem("useDetails", JSON.stringify(useDetails));
  return { data: useDetails, message: "Registration succesfully" };
};

const login = (username, password) => {
  let useDetails = JSON.parse(localStorage.getItem("useDetails"));
  let user = false;
  if (useDetails) {
    user = useDetails.find(
      (da) => da.username === username && da.password === password
    );
  }
  if (user) {
    localStorage.setItem("user", JSON.stringify(user));
    return user;
  } else {
    return user;
  }
};

const logout = () => {
  return (localStorage.user = false);
};

export default {
  register,
  login,
  logout,
};
