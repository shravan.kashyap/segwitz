const handleAddBlog = (dataUri, title, description, userName) => {
  let blogsData = JSON.parse(localStorage.getItem("blogsData"));
  let dat = { dataUri, title, description, userName, date: new Date() };
  if (blogsData) {
    blogsData.push(dat);
  } else {
    blogsData = [dat];
  }
  localStorage.setItem("blogsData", JSON.stringify(blogsData));
  return { data: blogsData, message: "Blog add succesfully" };
};

export default {
  handleAddBlog,
};
